import re
import bs4
import pandas as pd
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def getDate(blog_entry: bs4.element.Tag):
    """gets the release date of the blog entry

    Args:
        blog_entry (bs4.element.Tag): the blog entry to get the date for

    Raises:
        Warning: the date couldn't be found

    Returns:
        str: the release date, e.g. 08. August 2022
    """
    blog_date = blog_entry.find_next()
    blog_date_text = str(blog_date.get_text().strip())
    blog_date_text_splitted = blog_date_text.split('·')
    if len(blog_date_text_splitted) > 0:
        blog_date_final = blog_date_text_splitted[1]
    else:
        raise Warning('No Date found for: ' + blog_entry.name)
    print('Blog-Eintrag vom' + blog_date_final)
    return blog_date_final.strip()


def getTitle(blog_entry: bs4.element.Tag):
    """gets the title of the blog entry

    Args:
        blog_entry (bs4.element.Tag): the blog entry to get the title for

    Returns:
        str: the title, e.g. Tag 1
    """
    blog_title = blog_entry.find_next().find_next_sibling()
    print('Blog-Titel: ' + blog_title.get_text())
    return blog_title.get_text()


def getMaxDegree(blog_entry: bs4.element.Tag):
    """gets the max degree value of the blog entry

    Args:
        blog_entry (bs4.element.Tag): the blog entry to get the max degree value for

    Returns:
        int: the max degree value, e.g. 26
    """
    blog_max_degree = blog_entry.find_next().find_next_sibling().find_next_sibling()
    blog_max_degree_text = str(blog_max_degree.get_text().strip())
    blog_max_degree_text = re.findall(
        "max. \\d+ Grad", blog_max_degree_text)
    if len(blog_max_degree_text) > 0:
        numbers = [int(temp)
               for temp in blog_max_degree_text[0].split() if temp.isdigit()]
        max_degree = numbers[0]
        print('Höchsttemperatur: ' + max_degree.__str__())
        return max_degree
    else:
        print('Höchsttemperatur: Konnte nicht ermittelt werden')
        return 0


def getKilometers(blog_entry: bs4.element.Tag):
    """gets the kilometers driven of the blog entry

    Args:
        blog_entry (bs4.element.Tag): the blog entry to get the kilometers value for

    Returns:
        int: the kilometres driven, e.g. 200
    """
    blog_kilometers = blog_entry.find_next().find_next_sibling().find_next_sibling()
    blog_kilometers_text = str(blog_kilometers.get_text().strip())

    blog_kilometers_text = re.findall(
        "Kilometer: \\d+", blog_kilometers_text)
    if len(blog_kilometers_text) > 0:
        numbers = [int(temp)
               for temp in blog_kilometers_text[0].split() if temp.isdigit()]
        kilometers = numbers[0]
        print('Gefahrene Kilometer: ' + kilometers.__str__())
        return kilometers
    else:
        print('Gefahrene Kilometer: Konnte nicht ermittelt werden')
        return 0


def getDrivingTime(blog_entry: bs4.element.Tag):
    """gets the time driven of the blog entry

    Args:
        blog_entry (bs4.element.Tag): the blog entry to get the time driven for

    Returns:
        str: the time driven, eg. 06:30:00
    """
    blog_driving_time = blog_entry.find_next().find_next_sibling().find_next_sibling()
    blog_driving_time_text = str(blog_driving_time.get_text().strip())
    blog_driving_time = re.findall("\\d+ h \\d+ min", blog_driving_time_text)
    if len(blog_driving_time) > 0:
        numbers = [int(temp)
                   for temp in blog_driving_time[0].split() if temp.isdigit()]
        driving_time = datetime(2022, 8, 8, numbers[0], numbers[1], 0).time()
        print('Fahrdauer: ' + driving_time.__str__())
    else:
        driving_time = datetime(2022, 8, 8, 0, 0, 0).time()
        print('Fahrdauer: Konnte nicht ermittelt werden')
    return driving_time.__str__()

def load_all_entries(driver):
    """clicks 'Load more' till all entries are on the same page

    Args:
        driver (selenium.WebDriver): the driver clicking the button
    """
    load_more = True
    while load_more:
        try:
            driver.find_element(value='load-more-blog-posts').click()
            WebDriverWait(driver, 10)
        except:
            load_more = False


def get_driver(is_ci_run: bool):
    """gets the selenium driver

    Args:
        is_ci_run (bool): determines if remote or chrome webdriver is returned

    Returns:
        selenium.WebDriver: the webdriver for ci or local
    """
    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    if is_ci_run:
        return webdriver.Remote(
            command_executor='http://selenium__standalone-chrome:4444/wd/hub', options=options)
    else:
        return webdriver.Chrome(service=ChromeService(
            ChromeDriverManager().install()), options=options)


def get_blog(is_ci_run):
    """gets the blog

    Args:
        is_ci_run (bool): whether main.py got started in ci or not

    Returns:
        bs4.element.Tag: represents html of the blog with all entries
    """
    driver = get_driver(is_ci_run)
    driver.get("https://funralley2022-team-nordsee75.jimdofree.com/blog")
    driver.find_element(value='cookie-settings-reject-all').click()
    load_all_entries(driver)
    content = driver.page_source
    soup = bs4.BeautifulSoup(content, features="html.parser")
    return soup.find("div", attrs={'class': 'j-blog'})


def main(is_ci_run: bool):
    """scrapes the blog for blog entries and its values

    Args:
        is_ci_run (bool): whether main.py got started in ci or not

    Returns:
        pd.DataFrame: the scraped data
    """
    blog = get_blog(is_ci_run)
    blog_update = []
    blog_dates = []
    blog_titles = []
    blog_max_degree = []
    blog_kilometers = []
    blog_driving_times = []

    for blog_entry in blog.find_all("div", attrs={'id': 'cc-matrix-2'}):
        blog_dates.append(getDate(blog_entry))
        blog_titles.append(getTitle(blog_entry))
        blog_max_degree.append(getMaxDegree(blog_entry))
        blog_kilometers.append(getKilometers(blog_entry))
        blog_driving_times.append(getDrivingTime(blog_entry))
        blog_update.append("")
        print('-------------------')

    print('scraping finished')
    return pd.DataFrame({'Stand': blog_update, 'Tag': blog_dates, 'Titel': blog_titles,
                         "Kilometer": blog_kilometers, "Fahrdauer": blog_driving_times})
