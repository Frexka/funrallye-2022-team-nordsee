from datetime import datetime
import numpy
import pandas as pd
import setup

def calculateDaysSinceStart():
    """takes 08.08.2022 as startdate and returns difference to today

    Returns:
        _type_: difference to today (absolute)
    """
    start_date = datetime(2022, 8, 8).date()
    today = datetime.now().date()
    print('days since start calculated')
    return (today - start_date).days.__abs__()


def calculateKilometersSum(values: numpy.ndarray, kilometres_index: int):
    """adds up all kilometres values

    Args:
        values (numpy.ndarray): values list of scraped data
        kilometres_index (int): position index subtracted by 1 of kilometres column

    Returns:
        _type_: sum of all kilometres values
    """
    sum = 0
    for value in values.tolist():
        sum += value[kilometres_index]
    print('kilometres calculated')
    return sum

def getKilometresColumnIndex():
    ws_data = setup.get_worksheet("Daten")
    return ws_data.find("Kilometer").col - 1


def main(data: pd.DataFrame):
    days = calculateDaysSinceStart()
    kilometers_sum = calculateKilometersSum(data.values, getKilometresColumnIndex())
    return pd.DataFrame({'Stand': [""], 'Tage': [days], 'Kilometer (Gesamt)': [
                         kilometers_sum], 'Kilometer (Täglich)': [round(kilometers_sum / days, 2)]})