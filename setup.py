from array import array
import gspread
from uuid import uuid4
import requests
from dotenv import load_dotenv
import argparse
import os
import json


statistics_headers = ["Stand", "Tage", "Kilometer (Gesamt)", "Kilometer (Täglich)"]
data_headers = ["Stand", "Tag", "Titel", "Kilometer", "Fahrdauer"]


def get_arguments():
    """adds ```--is_ci_run={bool}``` as param to main.py

    Returns:
        namespace: returns is_ci_run false, if no param was entered, returns false
    """
    parser = argparse.ArgumentParser(description='is_ci_run')
    parser.add_argument('--is_ci_run', type=bool, default=False)
    return parser.parse_args()

def set_environment():
    """sets the environment variables if it is a local run -- environment.env neccessary"""
    if get_arguments().is_ci_run == False:
        load_dotenv(dotenv_path='environment.env')
        with open('service_account.json', 'r+') as file:
            data = json.load(file)
            data['private_key_id'] = data['private_key_id'].replace('$PRIVATE_KEY_ID', os.environ.get("private-key-id"))
            data['private_key'] = data['private_key'].replace('$PRIVATE_KEY', os.environ.get("private-key"))
            data['client_email'] = data['client_email'].replace('$CLIENT_EMAIL', os.environ.get("client-email"))
            data['client_id'] = data['client_id'].replace('$CLIENT_ID', os.environ.get("client-id"))
            data['client_x509_cert_url'] = data['client_x509_cert_url'].replace('$CLIENT_CERT_URL', os.environ.get("client-cert-url"))
        with open('service_account.json', 'w') as file:
            json.dump(data, file)
    else:
        print('CI RUN - Environment set via CI/CD environment variables')

def get_spreadsheet():
    """gets the funrallye spreadsheet

    Returns:
        Spreadsheet: contains all worksheets
    """
    sa = gspread.service_account(filename="service_account.json")
    return sa.open("FunRallye - Team Nordsee")


def get_worksheet(ws_name: str):
    """gets a worksheet from the funrallye spreadsheet

    Args:
        ws_name (str): the name of the worksheet

    Returns:
        Worksheet: the worksheet with ws_name as title
    """
    return get_spreadsheet().worksheet(ws_name)

def get_all_worksheets():
    """gets all funrallye spreadsheets

    Returns:
        list[Worksheet]: a list of all worksheets in the funrallye spreadsheet
    """
    return get_spreadsheet().worksheets()


def do_backup():
    """does a backup of all values of the funrallye spreadsheet

    !!!WARNING!!! currently only backups first worksheet - see: https://gitlab.com/Frexka/funrallye-2022-team-nordsee/-/issues/5
    """
    response = requests.get(
        'https://docs.google.com/spreadsheets/d/e/2PACX-1vSpCcBJnpk_eBp7TVy40dDOEEZPAc9Yp_6f9R5KCSVXHcPKZl_AQSWS4Jwu3uBcoyd7rkGWT1FE4f9n/pub?output=csv')
    response.raise_for_status()
    filePath = 'backup.csv'
    with open(filePath, 'a') as csvFile:
        csvFile.write(response.content.__str__())
    csvFile.close()
    print(filePath + ' created')


# TODO: implement func
def restore_latest_backup():
    """restores the latest backup.csv

    Raises:
        Exception: method is not implemented - see: https://gitlab.com/Frexka/funrallye-2022-team-nordsee/-/issues/2
    """
    raise Exception('restore_latest_backup not implemented')


def do_basic_setup(ws: gspread.Worksheet, headers: array):
    """deletes all rows and columns, adds the headers and formats them

    Args:
        ws (gspread.Worksheet): the worksheet to do the setup for
        headers (array): the header row
    """
    ws.clear()
    ws.delete_rows(1, ws.row_count - 1)
    ws.delete_columns(1, ws.col_count - 1)
    ws.append_row(headers)
    format_header(ws, 1)
    print("setup complete for: " + ws.title)

def format_header(ws: gspread.Worksheet, row: int):
    """formats the given row as a header

    Args:
        ws (gspread.Worksheet): the worksheet the row is in
        row (int): the row index to format as a header row
    """
    format = {
        "padding": {
            "top": 10,
            "left": 10,
            "right": 10,
            "bottom": 10,
        },
        "textFormat": {
            "fontSize": 12,
            "bold": True,
        },
        "horizontalAlignment": "CENTER",
        "verticalAlignment": "MIDDLE",
    }
    ws.format(str(row), format)

def format_row(ws: gspread.Worksheet, row: int):
    """formats the given row as a default row

    Args:
        ws (gspread.Worksheet): the worksheet the row is in
        row (int): the row index to format as a default row
    """
    format = {
        "padding": {
            "top": 5,
            "left": 5,
            "right": 5,
            "bottom": 5,
        },
        "textFormat": {
            "fontSize": 10,
            "bold": False,
        },
        "horizontalAlignment": "CENTER",
        "verticalAlignment": "MIDDLE",
    }
    ws.format(str(row), format)

def auto_resize_columns(ws: gspread.Worksheet):
    """auto resizes the columns of given worksheet

    Args:
        ws (gspread.Worksheet): the worksheet which columns get resized
    """
    body = {
        "requests": [
            {
                "autoResizeDimensions": {
                    "dimensions": {
                        "sheetId": ws._properties['sheetId'],
                        "dimension": "COLUMNS",
                        "startIndex": 0,
                        "endIndex": ws.col_count + 1
                    }
                }
            },
        ]
    }
    get_spreadsheet().batch_update(body)

def main():
    """basic setup for rows and columns for all worksheets"""
    if __name__ == "setup":
        do_backup()
        set_environment()
        statistics = get_worksheet("Statistiken")
        do_basic_setup(statistics, statistics_headers)
        data = get_worksheet("Daten")
        do_basic_setup(data, data_headers)
        print('setup finished')
