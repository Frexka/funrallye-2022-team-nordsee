# Funrallye 2022: Team Nordsee

## Project description

This python project was developed in connection with the funrallye 2022 - Innsbruch to Mallorca. Subject is the blog of team Nordsee with starting number 75.

This project automates:
- scraping the blog
- calculating statistics
- pushing data via gspread to google docs
- generating a backup before doing so

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Links

Blog:
- https://funralley2022-team-nordsee75.jimdofree.com/

Organizer:
- https://www.funrallye.at/mallorca


## Authors and acknowledgment

Author:
- Lukas Freesemann

## License

MIT License

Copyright (c) 2022 Lukas Freesemann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
