import setup
import scrape
import stats
import gspread
import numpy
from datetime import datetime


def append(ws: gspread.Worksheet, value: numpy.ndarray):
    """append an ndarray as new row to worksheet

    Args:
        ws (gspread.Worksheet): the worksheet to add the values to
        value (numpy.ndarray): the ndarray to add
    """
    ws.append_row(value.tolist())
    setup.format_row(ws, str(ws.row_count + 1))


def push_data():
    """adds scraped and calculated data to the according worksheets"""
    for value in data.values:
        append(ws_data, value)
    for value in statistics.values:
        append(ws_statistics, value)
    print('pushing data finished')


def update_timestamps():
    """adds a timestamp to all worksheets"""
    for worksheet in setup.get_all_worksheets():
        worksheet.update("A2", datetime.now().strftime(
            "%d.%m.%Y: %H:%M:%S").__str__())
    print('updating timestamps finished')


def resize_columns():
    """resizes the columns of all worksheets"""
    for worksheet in setup.get_all_worksheets():
        setup.auto_resize_columns(worksheet)
    print('resizing worksheets finished')


if __name__ == "__main__":
    setup.main()
    data = scrape.main(setup.get_arguments().is_ci_run)
    statistics = stats.main(data)
    ws_statistics = setup.get_worksheet("Statistiken")
    ws_data = setup.get_worksheet("Daten")
    push_data()
    update_timestamps()
    resize_columns()
    print('main finished')
